namespace Instructors.Domain
{
    public class Instructor : BaseEntity
    {
        public string FistName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        
    }
}