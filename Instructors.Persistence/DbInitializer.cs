﻿using System;
using System.Collections.Generic;
using System.Linq;
using Instructors.Domain;

namespace Instructors.Persistence
{
    public class DbInitializer
    {
        public static void Initialize(InstructorsDbContext context)
        {
            var initializer = new DbInitializer();
            initializer.SeedAll(context);
        }

        private void SeedAll(InstructorsDbContext context)
        {
            context.Database.EnsureCreated();

            if (context.Instructors.Any())
            {
                return; // Db has been seeded
            }

            SeedInstructors(context);
        }

        private void SeedInstructors(InstructorsDbContext context)
        {
            var instructors = new List<Instructor>();
            for (int i = 1; i <= 10; i++)
            {
                instructors.Add(new Instructor(
                    )
                {
                    FistName = "Fist Name " + i,
                    MiddleName = "Middle Name " + i,
                    LastName = "Last Name " + i,
                    CreatedDate = DateTime.UtcNow
                });
            }

            context.Instructors.AddRange(instructors);
            context.SaveChanges();
        }
    }
}
