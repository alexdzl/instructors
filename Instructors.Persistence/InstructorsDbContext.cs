﻿using Instructors.Domain;
using Microsoft.EntityFrameworkCore;

namespace Instructors.Persistence
{
    public class InstructorsDbContext : DbContext
    {
        public InstructorsDbContext(DbContextOptions<InstructorsDbContext> options): base(options)
        {
        }

        public DbSet<Instructor> Instructors { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(InstructorsDbContext).Assembly);
        }
    }
}
