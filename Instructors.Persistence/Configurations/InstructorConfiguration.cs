﻿using Instructors.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Instructors.Persistence.Configurations
{
    public class InstructorConfiguration : IEntityTypeConfiguration<Instructor>
    {
        public void Configure(EntityTypeBuilder<Instructor> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).HasColumnName("InstructorID");
            builder.Property(e => e.FistName)
                .HasMaxLength(50)
                .IsRequired();
            builder.Property(e => e.LastName)
                .HasMaxLength(50)
                .IsRequired();
        }
    }
}