﻿using System.Threading;
using System.Threading.Tasks;
using Instructors.App.Interfaces;
using Instructors.Domain;
using Instructors.Persistence.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Instructors.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private IAsyncRepository<Instructor> _instructorRepository;

        private readonly DbContext _databaseContext;
        public UnitOfWork(DbContext context)
        {
            _databaseContext = context;
        }

        public IAsyncRepository<Instructor> InstructorRepository =>
            _instructorRepository ?? (_instructorRepository = new EfGenericAsyncRepository<Instructor>(_databaseContext));

        public async Task<int> Commit(CancellationToken cancellationToken)
        {
            int save = await _databaseContext.SaveChangesAsync(cancellationToken);
            return await Task.FromResult(save);
        }
    }
}