import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InstructorsComponent } from './contact-form/contact-form.component';


const routes: Routes = [
  { path:'', redirectTo: '/home', pathMatch:'full'},
  { path:'contact', component: InstructorsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
