import { Component, OnInit} from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { 
  InstructorLookupModel,
  CreateInstructorCommand,
  InstructorLookupModelDetail,
  UpdateInstructorCommand 
} 
  from 'src/app/services/backend-api.generated.service';
import { ActivatedRoute } from '@angular/router';
import { InstructorsService } from '../instructors.service';

@Component({
  selector: 'app-instructor',
  templateUrl: './instructor.component.pug',
  styleUrls: ['./instructor.component.css']
})
export class InstructorComponent implements OnInit {
  form: FormGroup;
  id = ''

  constructor(
    private instructorsService: InstructorsService,
    private route: ActivatedRoute
    ) { }

  ngOnInit() {
    this.initForm(new InstructorLookupModel());
    this.route.params.subscribe(params => {
      this.id = params['id'];
      if(this.id){
        this.instructorsService.getById(this.id).subscribe(result => {
          this.initForm(result.instructor);
        });
      }
    });
  }
  
  private initForm(instructor:InstructorLookupModelDetail) {
    this.form = new FormGroup({
      fistName: new FormControl(instructor.fistName, [
        Validators.required,
       ]),
       middleName: new FormControl(instructor.middleName, [
        Validators.required,
        Validators.minLength(3)
      ]),
      lastName: new FormControl(instructor.lastName, [
        Validators.required,
        Validators.minLength(3)
      ])
    });
  }

  submit(){
      if (this.form.invalid) {
        return; 
      } 

      var formValue = {...this.form.value};
      console.log('FormData', formValue);
      
      if(!!this.id){
          let instructorUpdate = new UpdateInstructorCommand();
          instructorUpdate.init(formValue);
          this.instructorsService.update(this.id,instructorUpdate)
          .subscribe((response) => {
            this.postEdit();
        });
      }

      let instructorCreate = new CreateInstructorCommand();
      instructorCreate.init(formValue);
      this.instructorsService.create(instructorCreate)
        .subscribe((response) => {
          this.postEdit();
      });
  }
  postEdit() {
    this.instructorsService.getAll();
    this.form.reset();
  }
}
