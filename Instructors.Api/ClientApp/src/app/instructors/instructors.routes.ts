import { Routes, RouterModule } from "@angular/router"
import { InstructorsComponent } from "./instructors/instructors.component"
import { InstructorComponent } from "./instructor/instructor.component"

const InstructorsRoutes: Routes = [
    {
        path: "instructors",
        component: InstructorsComponent,
        children:[
            {
                path: 'add', 
                component: InstructorComponent,
            },
            {
                path: ':id', 
                component: InstructorComponent,
            }
        ]
    }
  ] 
  
export const instructorsRouting = RouterModule.forChild(InstructorsRoutes)