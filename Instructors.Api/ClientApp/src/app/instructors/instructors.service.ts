import { Injectable } from '@angular/core';
import { InstructorLookupModel, InstructorsClient, CreateInstructorCommand, UpdateInstructorCommand, InstructorViewModel } from '../services/backend-api.generated.service';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InstructorsService {
  constructor(private instructorsClient:InstructorsClient) { }

  private loading = new BehaviorSubject<boolean>(false);
  private error = new BehaviorSubject<string>('');
  private _instructors = new BehaviorSubject<InstructorLookupModel[]>([]);
  private dataStore: 
    {
     instructors: InstructorLookupModel[]
    }
     = 
    {
      instructors: []
    }
  readonly instructors = this._instructors.asObservable();

  get loadingState(){
    return this.loading.asObservable();
  }

  get getError(){
    return this.error.asObservable();
  }

  get getInstructors(){
    return this._instructors.asObservable();
  }

  getById(id): Observable<InstructorViewModel>{
      return this.instructorsClient.get(id);
  }


  delete(id:string) {
    this.instructorsClient
           .delete(id)
           .subscribe((response) => {
            this.getAll();
         });
  }

  create(instructor:CreateInstructorCommand){
     return this.instructorsClient.create(instructor); 
  }

  update(id:any,instructor:UpdateInstructorCommand){
    return this.instructorsClient.update(id,instructor);
  }

  public getAll() {
    this.loading.next(true);
    this.instructorsClient.getAll()
      .subscribe(responce => {
        console.log('responce', responce);
        
        this.dataStore.instructors = responce.instructors;
        this._instructors.next(Object.assign({}, this.dataStore).instructors);
    
        this.loading.next(false);
      }, error => {
        this.error.next(error.message) ;
      });
  }
}
