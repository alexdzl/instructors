import { Component, OnInit } from '@angular/core';
import { InstructorLookupModel } from '../../services/backend-api.generated.service';
import { InstructorsService } from '../instructors.service';

@Component({
  selector: 'app-instructors',
  templateUrl: './instructors.component.pug',
  styleUrls: ['./instructors.component.css']
})
export class InstructorsComponent implements OnInit {
  loading = false;
  instructors: InstructorLookupModel[];
  error = '';

  constructor(private instructorsService: InstructorsService) { }

  ngOnInit() {
    this.instructorsService.getInstructors
      .subscribe(instructors => {
        this.instructors = instructors;
    });
    this.instructorsService.getAll();
  }

  delete(id:string) {
    this.instructorsService.delete(id);
 }
}
