import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InstructorsComponent } from './instructors/instructors.component';
import { InstructorComponent } from './instructor/instructor.component';
import { instructorsRouting } from './instructors.routes';
import { InstructorsClient } from '../services/backend-api.generated.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InstructorsService } from './instructors.service';


@NgModule({
  declarations: [
    InstructorsComponent,
    InstructorComponent,
  ],
  imports: [
    CommonModule,
    instructorsRouting,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports:[
    InstructorsComponent,
    InstructorComponent
  ],
  providers:[InstructorsClient, InstructorsService]
})
export class InstructorsModule { }
