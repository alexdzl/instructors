﻿using System.Threading.Tasks;
using Instructors.App.Interfaces;
using Instructors.App.Notifications;

namespace Instructors.Infrastructure
{
    class NotificationService : INotificationService
    {
        public Task SendAsync(Message message)
        {
            return Task.CompletedTask;
        }
    }
}
