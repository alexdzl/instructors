﻿using AutoMapper;

namespace Instructors.App.Interfaces.Mapping
{
    public interface ICustomMapping
    {
        void CreateMappings(Profile configuration);
    }
}
