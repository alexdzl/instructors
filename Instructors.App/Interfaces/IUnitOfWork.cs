﻿using System.Threading;
using System.Threading.Tasks;
using Instructors.Domain;

namespace Instructors.App.Interfaces
{
    public interface IUnitOfWork
    {
        IAsyncRepository<Instructor> InstructorRepository { get; }
        Task<int> Commit(CancellationToken cancellationToken);
    }
}
