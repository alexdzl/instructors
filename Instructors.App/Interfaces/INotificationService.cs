﻿using System.Threading.Tasks;
using Instructors.App.Notifications;

namespace Instructors.App.Interfaces
{
    public interface INotificationService
    {
        Task SendAsync(Message message);
    }
}
