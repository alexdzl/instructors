﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Instructors.Domain;

namespace Instructors.App.Interfaces
{
    public interface IAsyncRepository<T> where T : BaseEntity
    {

        Task<T> GetById(Guid id);
        Task<T> FirstOrDefault(Expression<Func<T, bool>> predicate);

        Task Add(T entity);
        void Update(T entity);
        void Remove(T entity);

        Task<IEnumerable<T>> Get();
        Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate);

        Task<int> Count();
        Task<int> CountWhere(Expression<Func<T, bool>> predicate);

    }
}