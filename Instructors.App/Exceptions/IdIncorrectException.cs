﻿namespace Instructors.App.Exceptions
{
    public class IdIncorrectException : ApplicationException
    {
        public IdIncorrectException(string name, object key)
            : base($"Id incorrect \"{name}\" ({key}).")
        {
        }
    }
}