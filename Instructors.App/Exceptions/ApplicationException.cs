﻿using System;

namespace Instructors.App.Exceptions
{
    public class ApplicationException : Exception
    {
        public ApplicationException(string message)
            : base(message) { }
    }
}