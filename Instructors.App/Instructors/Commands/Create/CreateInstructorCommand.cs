using System;
using System.Threading;
using System.Threading.Tasks;
using Instructors.App.Interfaces;
using Instructors.Common;
using Instructors.Domain;
using MediatR;

namespace Instructors.App.Instructors.Commands.Create
{
    public class CreateInstructorCommand : IRequest
    {
        public string FistName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        public class Handler : IRequestHandler<CreateInstructorCommand, Unit>
        {
            private readonly IUnitOfWork _unitOfWork;
            private readonly IDateTime _sourceDateTime;

            public Handler(IUnitOfWork unitOfWork, IDateTime sourceDateTime)
            {
                _unitOfWork = unitOfWork;
                _sourceDateTime = sourceDateTime;
            }

            public async Task<Unit> Handle(CreateInstructorCommand request, CancellationToken cancellationToken)
            {
                var entity = new Instructor
                {
                    Id = Guid.NewGuid(),
                    FistName = request.FistName,
                    LastName = request.LastName,
                    MiddleName = request.MiddleName,
                    CreatedDate = _sourceDateTime.UtcNow
                };

                await _unitOfWork.InstructorRepository.Add(entity);

                await _unitOfWork.Commit(cancellationToken);

                return Unit.Value;
            }
        }
    }
}