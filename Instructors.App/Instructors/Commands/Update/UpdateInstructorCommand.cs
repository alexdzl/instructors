﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Instructors.App.Exceptions;
using Instructors.App.Interfaces;
using Instructors.Domain;
using MediatR;

namespace Instructors.App.Instructors.Commands.Update
{
    public class UpdateInstructorCommand : IRequest
    {
        public string Id { get; set; }
        public string FistName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        public class Handler : IRequestHandler<UpdateInstructorCommand, Unit>
        {
            private readonly IUnitOfWork _unitOfWork;

            public Handler(IUnitOfWork unitOfWork)
            {
                this._unitOfWork = unitOfWork;
            }

            public async Task<Unit> Handle(UpdateInstructorCommand request, CancellationToken cancellationToken)
            {
                if (Guid.TryParse(request.Id, out var id))
                {
                    var entity = await _unitOfWork.InstructorRepository.GetById(id);
                    if (entity == null)
                    {
                        throw new NotFoundException(nameof(Instructor), request.Id);
                    }

                    entity.FistName = request.FistName;
                    entity.LastName = request.LastName;
                    entity.MiddleName = request.MiddleName;

                    _unitOfWork.InstructorRepository.Update(entity);

                    await _unitOfWork.Commit(cancellationToken);

                    return Unit.Value;
                }
                throw new IdIncorrectException(nameof(Instructor), request.Id);
            }
        }
    }
}