﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Instructors.App.Exceptions;
using Instructors.App.Interfaces;
using Instructors.Domain;
using MediatR;

namespace Instructors.App.Instructors.Commands.Delete
{
    public class DeleteInstructorCommand: IRequest
    {
        public string Id { get; set; }

        public class Handler : IRequestHandler<DeleteInstructorCommand, Unit>
        {
            private IUnitOfWork _unitOfWork;

            public Handler(IUnitOfWork unitOfWork)
            {
                this._unitOfWork = unitOfWork;
            }

            public async Task<Unit> Handle(DeleteInstructorCommand request, CancellationToken cancellationToken)
            {
                if (Guid.TryParse(request.Id, out var id))
                {
                    var entity = await _unitOfWork.InstructorRepository.GetById(id);
                    if (entity == null)
                    {
                        throw new NotFoundException(nameof(Instructor), request.Id);
                    }

                    _unitOfWork.InstructorRepository.Remove(entity);

                    await _unitOfWork.Commit(cancellationToken);

                    return Unit.Value;
                }
                throw new IdIncorrectException(nameof(Instructor), request.Id);
            }
        }
    }
}