﻿using System.Collections.Generic;

namespace Instructors.App.Instructors.Queries.GetAll
{
    public class InstructorsListViewModel
    {
        public IEnumerable<InstructorLookupModel> Instructors { set; get; }
    }
}