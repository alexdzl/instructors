﻿using MediatR;

namespace Instructors.App.Instructors.Queries.GetAll
{
    public class GetInstructorsListQuery : IRequest<InstructorsListViewModel>
    {
    }
}