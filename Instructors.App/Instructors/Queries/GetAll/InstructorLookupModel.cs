﻿namespace Instructors.App.Instructors.Queries.GetAll
{
    public class InstructorLookupModel
    {
        public string Id { set; get; }
        public string FistName { get; set; }
        public string LastName { get; set; }
    }
}