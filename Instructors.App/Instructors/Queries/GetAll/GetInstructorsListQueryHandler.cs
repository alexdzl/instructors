﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Instructors.App.Interfaces;
using MediatR;

namespace Instructors.App.Instructors.Queries.GetAll
{
    public class GetInstructorsListQueryHandler : IRequestHandler<GetInstructorsListQuery, InstructorsListViewModel>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetInstructorsListQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<InstructorsListViewModel> Handle(GetInstructorsListQuery request, CancellationToken cancellationToken)
        {
            return new InstructorsListViewModel()
            {
                Instructors = await GetViewModels()
            };
        }

        private async Task<List<InstructorLookupModel>> GetViewModels()
        {
            var entities = await _unitOfWork.InstructorRepository.Get();
            return _mapper.Map<List<InstructorLookupModel>>(entities);
        }
    }
}