﻿namespace Instructors.App.Instructors.Queries.Get
{
    public class InstructorLookupModelDetail
    {
        public string Id { set; get; }
        public string FistName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
    }
}