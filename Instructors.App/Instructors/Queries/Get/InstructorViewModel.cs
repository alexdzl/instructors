﻿namespace Instructors.App.Instructors.Queries.Get
{
    public class InstructorViewModel
    {
        public InstructorLookupModelDetail Instructor { set; get; }
    }
}