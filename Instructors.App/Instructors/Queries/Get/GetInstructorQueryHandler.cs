﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Instructors.App.Exceptions;
using Instructors.App.Interfaces;
using Instructors.Domain;
using MediatR;

namespace Instructors.App.Instructors.Queries.Get
{
    public class GetInstructorQueryHandler : IRequestHandler<GetInstructorQuery, InstructorViewModel>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetInstructorQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<InstructorViewModel> Handle(GetInstructorQuery request, CancellationToken cancellationToken)
        {
            return new InstructorViewModel()
            {
                Instructor = await GetViewModels(request.Id)
            };
        }

        private async Task<InstructorLookupModelDetail> GetViewModels(string requestId)
        {
            if (Guid.TryParse(requestId, out var id))
            {
                var entity = await _unitOfWork.InstructorRepository.GetById(id);
                if (entity == null)
                {
                    throw new NotFoundException(nameof(Instructor), requestId);
                }
                return _mapper.Map<InstructorLookupModelDetail>(entity);
            }
            throw new IdIncorrectException(nameof(Instructor), requestId);
        }
    }
}