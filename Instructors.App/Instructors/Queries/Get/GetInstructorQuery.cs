﻿using MediatR;

namespace Instructors.App.Instructors.Queries.Get
{
    public class GetInstructorQuery : IRequest<InstructorViewModel>
    {
        public string Id { set; get; }
    }
}