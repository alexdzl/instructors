using Instructors.App.Interfaces;
using MediatR;
using Moq;
using NUnit.Framework;

namespace Instructors.Tests
{
    public class CommandTestBase
    {
        protected Mock<IMediator> _mediatorMock;
        protected Mock<IUnitOfWork> _unitOfWork;

        [SetUp]
        public void Setup()
        {
            _mediatorMock = new Mock<IMediator>();
            _unitOfWork = new Mock<IUnitOfWork>();
        }
    }
}