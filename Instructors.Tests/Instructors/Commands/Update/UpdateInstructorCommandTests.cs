﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Instructors.App.Exceptions;
using Instructors.App.Instructors.Commands.Update;
using Instructors.App.Interfaces;
using Instructors.Domain;
using Moq;
using NUnit.Framework;

namespace Instructors.Tests.Instructors.Commands.Update
{
    class UpdateInstructorCommandTests: CommandTestBase
    {
        private Instructor _instructor;
        private List<Instructor> _instructors;

        [SetUp]
        public void Setup()
        {
            _instructor = new Instructor()
            {
                FistName = "Name",
                LastName = "LastName"
            };
            _instructors = new List<Instructor> {_instructor};

            Mock<IAsyncRepository<Instructor>> repositoryMock = new Mock<IAsyncRepository<Instructor>>();
            _unitOfWork.Setup(uow => uow.InstructorRepository).Returns(repositoryMock.Object);

            repositoryMock.Setup(r => r.GetById(It.IsAny<Guid>())).Returns<Guid>((id) =>
            {
                return Task.FromResult(_instructors.FirstOrDefault(instructor => instructor.Id == id)) ;
            });

            repositoryMock.Setup(r => r.Update(It.IsAny<Instructor>())).Callback<Instructor>(instructor =>
            {
                var entity = _instructors.FirstOrDefault(inst => inst.Id == instructor.Id);
                entity.FistName = instructor.FistName;
                entity.LastName = instructor.LastName;
            });
        }

        [Test]
        public void Handle_ValidUpdateRequest_ShouldChangeInstructor()
        {
            // Arrange
            var sut = new UpdateInstructorCommand.Handler(_unitOfWork.Object);
            string expectId = "8912e343-9769-4acd-a879-0ecae863ca40";
            string expectFistName = "FistNameC";
            string expectLastName = "LastNameC";
            string expectMiddleName = "MiddleNameC";
            _instructor.Id = Guid.Parse(expectId);

            // Act
            var result = sut.Handle(new UpdateInstructorCommand
            {
                FistName = expectFistName,
                LastName = expectLastName,
                MiddleName = expectMiddleName,
                Id = expectId
            }, CancellationToken.None);

            var instructor = _instructors.First();
            // Assert
            Assert.AreEqual(expectFistName, instructor.FistName);
            Assert.AreEqual(expectLastName, instructor.LastName);
            Assert.AreEqual(expectMiddleName, instructor.MiddleName);
        }

        [Test]
        public void Handle_InvalidDeleteRequest_ShouldThrowIdIncorrectException()
        {
            // Arrange
            var sut = new UpdateInstructorCommand.Handler(_unitOfWork.Object);
            string invalidId = "";

            // Act
            // Assert
            Assert.ThrowsAsync<IdIncorrectException>(async () => await sut.Handle(new UpdateInstructorCommand
            {
                Id = invalidId
            }, CancellationToken.None));
        }

        [Test]
        public void Handle_UpdateRequestWithNonExistingId_ShouldThrowNotFoundException()
        {
            // Arrange
            var sut = new UpdateInstructorCommand.Handler(_unitOfWork.Object);
            string nonExistingId = "1111e343-9769-4acd-a879-0ecae863ca40";
 
            // Act
            // Assert
            Assert.ThrowsAsync<NotFoundException>(async () => await sut.Handle(new UpdateInstructorCommand
            {
                Id = nonExistingId
            }, CancellationToken.None));
        }
    }
}
