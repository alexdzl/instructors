﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Instructors.App.Exceptions;
using Instructors.App.Instructors.Commands.Delete;
using Instructors.App.Interfaces;
using Instructors.Domain;
using Moq;
using NUnit.Framework;

namespace Instructors.Tests.Instructors.Commands.Delete
{
    class DeleteInstructorCommandTests: CommandTestBase
    {
        private Instructor _instructor;
        private List<Instructor> _instructors;

        [SetUp]
        public void Setup()
        {
            _instructor = new Instructor()
            {
                FistName = "Name",
                LastName = "LastName"
            };
            _instructors = new List<Instructor> {_instructor};

            Mock<IAsyncRepository<Instructor>> repositoryMock = new Mock<IAsyncRepository<Instructor>>();
            _unitOfWork.Setup(uow => uow.InstructorRepository).Returns(repositoryMock.Object);

            repositoryMock.Setup(r => r.GetById(It.IsAny<Guid>())).Returns<Guid>((id) =>
            {
                return Task.FromResult(_instructors.FirstOrDefault(instructor => instructor.Id == id)) ;
            });

            repositoryMock.Setup(r => r.Remove(It.IsAny<Instructor>())).Callback<Instructor>(instructor =>
                {
                    _instructors.Remove(instructor);
                });
        }

        [Test]
        public void Handle_ValidDeleteRequest_ShouldRemoveInstructor()
        {
            // Arrange
            var sut = new DeleteInstructorCommand.Handler(_unitOfWork.Object);
            string expectId = "8912e343-9769-4acd-a879-0ecae863ca40";
            _instructor.Id = Guid.Parse(expectId);

            // Act
            var result = sut.Handle(new DeleteInstructorCommand
            {
                Id = expectId
            }, CancellationToken.None);

            // Assert
            Assert.That(_instructors, Has.Count.EqualTo(0));
        }

        [Test]
        public void Handle_InvalidDeleteRequest_ShouldThrowIdIncorrectException()
        {
            // Arrange
            var sut = new DeleteInstructorCommand.Handler(_unitOfWork.Object);
            string invalidId = "";

            // Act
            // Assert
            Assert.ThrowsAsync<IdIncorrectException>(async () => await sut.Handle(new DeleteInstructorCommand
            {
                Id = invalidId
            }, CancellationToken.None));
        }

        [Test]
        public void Handle_DeleteRequestWithNonExistingId_ShouldThrowNotFoundException()
        {
            // Arrange
            var sut = new DeleteInstructorCommand.Handler(_unitOfWork.Object);
            string nonExistingId = "1111e343-9769-4acd-a879-0ecae863ca40";
 
            // Act
            // Assert
            Assert.ThrowsAsync<NotFoundException>(async () => await sut.Handle(new DeleteInstructorCommand
            {
                Id = nonExistingId
            }, CancellationToken.None));
        }
    }
}
