using System;
using System.Threading;
using System.Threading.Tasks;
using Instructors.App.Instructors.Commands.Create;
using Instructors.App.Interfaces;
using Instructors.Domain;
using Instructors.Tests.Stubs;
using Moq;
using NUnit.Framework;

namespace Instructors.Tests.Instructors.Commands.Create
{
    public class CreateInstructorCommandTests : CommandTestBase
    {
        private Instructor _instructor;

        [SetUp]
        public void Setup()
        {
            _instructor = null;

            Mock<IAsyncRepository<Instructor>> repositoryMock = new Mock<IAsyncRepository<Instructor>>();

            _unitOfWork.Setup(uow => uow.InstructorRepository).Returns(repositoryMock.Object);

            repositoryMock.Setup(repo => repo.Add(It.IsAny<Instructor>())).Returns<Instructor>((instructor) =>
            {
                _instructor = instructor;
                return Task.CompletedTask;
            });
        }

        [Test]
        public void Handle_GivenValidRequest_ShouldCreateInstructorEntity()
        {
            // Arrange
            DateTime expectTime = new DateTime(2000,1,1,0,0,0,DateTimeKind.Utc);
            var sut = new CreateInstructorCommand.Handler(_unitOfWork.Object, new DateTimeStub(expectTime));
            string expectFistName = "FistName";
            string expectLastName = "LastName";
            string expectMiddleName = "MiddleName";

            // Act
            var result = sut.Handle(new CreateInstructorCommand()
            {
                FistName = expectFistName,
                LastName = expectLastName,
                MiddleName = expectMiddleName,
            }, CancellationToken.None);
            var actualInstructor = _instructor;

            // Assert
            Assert.AreNotEqual(Guid.Empty, actualInstructor.Id);
            Assert.AreEqual(expectFistName, actualInstructor.FistName);
            Assert.AreEqual(expectLastName, actualInstructor.LastName);
            Assert.AreEqual(expectMiddleName, actualInstructor.MiddleName);
            Assert.AreEqual(expectTime, actualInstructor.CreatedDate);
        }
    }
}

