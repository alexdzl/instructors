﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Instructors.App.Exceptions;
using Instructors.App.Infrastructure.AutoMapper;
using Instructors.App.Instructors.Queries.Get;
using Instructors.App.Interfaces;
using Instructors.Domain;
using Moq;
using NUnit.Framework;

namespace Instructors.Tests.Instructors.Queries.Get
{
    class GetInstructorQueryHandlerTests
    {
        protected Mock<IUnitOfWork> _unitOfWork;
        protected IMapper _mapper;

        private Instructor _instructor;
        private List<Instructor> _instructors;

        [SetUp]
        public void Setup()
        {
            _unitOfWork = new Mock<IUnitOfWork>();

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapperProfile());
            });

            _instructor = new Instructor()
            {
                FistName = "Name",
                LastName = "LastName"
            };
            _instructors = new List<Instructor> { _instructor };

            Mock<IAsyncRepository<Instructor>> repositoryMock = new Mock<IAsyncRepository<Instructor>>();
            _unitOfWork.Setup(uow => uow.InstructorRepository).Returns(repositoryMock.Object);

            repositoryMock.Setup(r => r.GetById(It.IsAny<Guid>())).Returns<Guid>((id) =>
            {
                return Task.FromResult(_instructors.FirstOrDefault(instructor => instructor.Id == id));
            });


            _mapper = mappingConfig.CreateMapper();
        }

        [Test]
        public async Task Handle_QueryRequest_ShouldReturnInstructor()
        {
            // Arrange
            var sut = new GetInstructorQueryHandler(_unitOfWork.Object, _mapper);
            string expectId = "8912e343-9769-4acd-a879-0ecae863ca40";
            _instructor.Id = Guid.Parse(expectId);

            // Act
            var result = await sut.Handle(new GetInstructorQuery()
            {
                Id = expectId
            } ,CancellationToken.None);

            // Assert
            Assert.IsInstanceOf<InstructorViewModel>(result);
            Assert.AreEqual(expectId, result.Instructor.Id);
        }

        [Test]
        public void Handle_InvalidGetRequest_ShouldThrowIdIncorrectException()
        {
            // Arrange
            var sut = new GetInstructorQueryHandler(_unitOfWork.Object, _mapper);
            string invalidId = "";

            // Act
            // Assert
            Assert.ThrowsAsync<IdIncorrectException>(async () => await sut.Handle(new GetInstructorQuery
            {
                Id = invalidId
            }, CancellationToken.None));
        }

        [Test]
        public void Handle_GetRequestWithNonExistingId_ShouldThrowNotFoundException()
        {
            // Arrange
            var sut = new GetInstructorQueryHandler(_unitOfWork.Object, _mapper);
            string nonExistingId = "1111e343-9769-4acd-a879-0ecae863ca40";

            // Act
            // Assert
            Assert.ThrowsAsync<NotFoundException>(async () => await sut.Handle(new GetInstructorQuery
            {
                Id = nonExistingId
            }, CancellationToken.None));
        }
    }
}
