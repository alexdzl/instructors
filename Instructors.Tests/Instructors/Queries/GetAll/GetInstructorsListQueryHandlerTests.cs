﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Instructors.App.Infrastructure.AutoMapper;
using Instructors.App.Instructors.Queries.GetAll;
using Instructors.App.Interfaces;
using Instructors.Domain;
using Moq;
using NUnit.Framework;

namespace Instructors.Tests.Instructors.Queries.GetAll
{
    class GetInstructorsListQueryHandlerTests
    {
        protected Mock<IUnitOfWork> _unitOfWork;
        protected IMapper _mapper;

        [SetUp]
        public void Setup()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapperProfile());
            });
            _unitOfWork = new Mock<IUnitOfWork>();

            Mock<IAsyncRepository<Instructor>> repositoryMock = new Mock<IAsyncRepository<Instructor>>();
            _unitOfWork.Setup(uow => uow.InstructorRepository).Returns(repositoryMock.Object);
            repositoryMock.Setup(r => r.Get()).ReturnsAsync(() =>
            {
                var list = new List<Instructor>
                {
                    new Instructor() {FistName = "fName1", LastName = "LName1"},
                    new Instructor() {FistName = "fName2", LastName = "LName2"}
                };

                return list;
            });

            _mapper = mappingConfig.CreateMapper();
        }

        [Test]
        public async Task Handle_QueryRequest_ShouldReturnList()
        {
            // Arrange
            var sut = new GetInstructorsListQueryHandler(_unitOfWork.Object, _mapper);

            // Act
            var result = await sut.Handle(new GetInstructorsListQuery() ,CancellationToken.None);

            // Assert
            Assert.IsInstanceOf<InstructorsListViewModel>(result);
            Assert.That(result.Instructors, Has.Count.EqualTo(2));
        }
    }
}
